import java.util.Scanner;

public class ParadoxProblemA {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int numberOfDataSets = sc.nextInt();
            if (numberOfDataSets > 0) {
                int previousElapsedHours = 0;
                int milesSum = 0;
                for (int i = 1; i <= numberOfDataSets; i++) {
                    int speed = sc.nextInt();
                    int elapsedHours = sc.nextInt();
                    int hoursAtThatSpeed = elapsedHours - previousElapsedHours;
                    previousElapsedHours = elapsedHours;

                    milesSum += speed * hoursAtThatSpeed;
                }
                System.out.println(milesSum + " miles");
            }
        }
    }

}
