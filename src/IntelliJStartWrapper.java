import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class IntelliJStartWrapper {

    public static void main(String[] args) throws IOException {
        FileInputStream is = new FileInputStream(new File("resources/sample.in"));
        System.setIn(is);
        ParadoxProblemA.main(args);
    }

}
